from m5stack import lcd
import utime
import motor 

lcd.clear()
lcd.setCursor(0, 0)
lcd.setColor(lcd.WHITE)
lcd.print("StepMotor Test: ")

stepmotor_0 = motor.StepMotor(0x70)
stepmotor_1 = motor.StepMotor(0x71)

while True:
    stepmotor_0.StepMotor_XYZ(0, 0, 0, 500)
    stepmotor_1.StepMotor_XYZ(0, 0, 0, 500)
    utime.sleep(3)
    stepmotor_0.StepMotor_XYZ(10.5, 10.5, 10.5, 500)
    stepmotor_1.StepMotor_XYZ(10.5, 10.5, 10.5, 500)
    utime.sleep(3)
